import Gallery from "./Layouts/Gallery"
import Header from "./Layouts/Header"
import { Provider } from "react-redux"
import store from "./store"
import Footer from "./Layouts/Footer"

function App() {
  return (
    <Provider store={store}>
      <Header />
      <Gallery />
      <Footer />
    </Provider>
  )
}

export default App
