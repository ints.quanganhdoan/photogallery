import React, { useRef } from 'react';
import Button from '../Components/Button'
import { useAppDispatch, useAppSelector } from '../hooks';
import { fetchPhotos, setCurrentSearch } from '../Slices/photoSlice';
import { FaSearch } from 'react-icons/fa';
import { RootState } from '../store';

const SearchBox : React.FC = () => {

    const {currentSearch} = useAppSelector((state : RootState) => state.photos);

    const inputRef = useRef<HTMLInputElement>(null);

    const dispatch = useAppDispatch();

    const handleSearch : () => void = () => {
        if(inputRef.current) {
            dispatch(setCurrentSearch(inputRef.current.value));
        }
        dispatch(fetchPhotos(currentSearch));
    }

    const handleKeyPress : (e: React.KeyboardEvent) => void = (e) => {
        if(e.key === 'Enter'){
            if(inputRef.current) {
                dispatch(setCurrentSearch(inputRef.current.value));
            }
          dispatch(fetchPhotos(currentSearch));
        }
      }

    return (
        <div className='relative'>
            <input 
            placeholder='Search image here...'
            className='rounded-sm w-64 h-8 indent-4 pr-16 text-xs 
            sm:text-sm sm:h-9 sm:w-96
            md:text-base md:h-10 md:w-[420px]'
            onKeyDown={handleKeyPress}
            ref={inputRef}>
            </input>
            <Button
            className='absolute right-0 top-0 bg-lightGray px-4 h-8 rounded-r-sm text-white text-xs hover:opacity-70 gap-0
            sm:h-9 sm:px-5 sm:text-sm
            md:h-10 md:px-7 md:text-base'
            onClick={handleSearch}
            >
                <FaSearch />
            </Button>
        </div>
    );
};

export default SearchBox;