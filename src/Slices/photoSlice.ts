import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
import axios from "axios";
import { baseUrl, accessKey } from "../enviroment";

interface Photo {
    id: string,
    description: string,
    alt_description: string
    urls: {
        full: string,
    },   
}

interface PhotoState {
    photos: Photo[],
    currentSearch: string
}

export const fetchPhotos = createAsyncThunk(
    'photos/fetchPhotosByName',
    async (name : string) => {
        const firstResponse = await axios.get(`${baseUrl}/search/photos?query=${name}&page=1&per_page=30`, {
            headers: {
                Authorization: `Client-ID ${accessKey}`
            }
        });

        const secondResponse = await axios.get(`${baseUrl}/search/photos?query=${name}&page=2&per_page=30`, {
            headers: {
                Authorization: `Client-ID ${accessKey}`
            }
        });
        const response = await Promise.all([firstResponse, secondResponse]);
        const responseData = [...response[0].data.results, ...response[1].data.results]
    return responseData;
    }
)

const photoSlice = createSlice({
    name: 'photos',
    initialState: {
        photos: [],
        currentSearch: ''
    } as PhotoState, 
    reducers: {
        setCurrentSearch: (state: PhotoState, action: PayloadAction<string>) => {
            state.currentSearch = action.payload;
        }
    },
    extraReducers: (builder) => {
        builder.addCase(fetchPhotos.fulfilled, (state: PhotoState, action: PayloadAction<Photo[]>) => {
            state.photos = action.payload;
        });
    }
})

export const { setCurrentSearch } = photoSlice.actions;

export default photoSlice.reducer;

