const Footer = () => {
    return (
        <div className='w-full flex justify-center items-center h-10 bg-darkGray font-light text-white text-xs
        sm:text-sm sm:h-12
        md:text-base md:h-14'
        >
            Copyright 2023 © Quang Anh 
        </div>
    );
};

export default Footer;