import SearchBox from '../Components/SearchBox';
import { FaCameraRetro } from 'react-icons/fa';

const Header = () => {
    return (
        <header className='flex flex-col justify-center items-center h-40 bg-darkGray gap-y-5 
        sm:h-44'>
            <div className='flex flex-col items-center gap-y-1'>
                <FaCameraRetro className='text-white text-2xl 
                sm:text-3xl
                md:text-4xl'/>
                <h1 className='text-white text-xl font-bold 
                sm:text-2xl
                md:text-3xl'>Photograph</h1>
            </div>
            <SearchBox />
        </header>
    );
};

export default Header;