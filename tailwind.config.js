/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        darkGray: "#282C34",
        lightGray: "#6d6d6d"
      }
    },
  },
  plugins: [],
}

